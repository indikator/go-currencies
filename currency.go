package currencies

import (
	"encoding/xml"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

const (
	lbURL         = "http://lb.lt/webservices/FxRates/FxRates.asmx"
	ratesURL      = lbURL + "/getFxRatesForCurrency?tp=EU" // &ccy=RUB&dtFrom=2019-03-01&dtTo=2019-04-02
	currenciesURL = lbURL + "/getCurrencyList"
)

var (
	rates      map[string]float64
	currencies map[string]string
)

// Rate - structure of currency's rate on date with spend time for getting
type Rate struct {
	Currency    string
	Rate        float64
	Date        string
	RunningTime float64
}

// GetRate - function to get currency rate for "today" based on "EUR"
// currency - ISO4217 currency code. Example, "USD", "EUR", "RUB"
func GetRate(currency string) (*Rate, error) {
	now := time.Now()
	date := now.Format("2006-01-02")
	return GetRateOnDate(currency, date)
}

// GetRateOnDate - function to get currency rate for date, based on "EUR"
// currency - ISO4217 currency code. Example, "USD", "EUR", "RUB"
// date - date in format "YYYY-MM-DD"
func GetRateOnDate(currency, date string) (*Rate, error) {
	r := &Rate{}
	if len(currency) != 3 {
		return r, errors.New("wrong currency format")
	}

	if _, err := time.Parse("2006-01-02", date); err != nil {
		return r, fmt.Errorf("wrong date or date format: %s", err.Error())
	}

	r.Currency = currency
	r.Date = date
	timeStart := time.Now()

	if rates == nil {
		rates = make(map[string]float64)
	}

	if currency == "EUR" {
		return &Rate{"EUR", 1, date, 0}, nil
	}

	if val, ok := rates[currency+"."+date]; ok {
		r.Rate = val
		r.RunningTime = time.Since(timeStart).Seconds()
		return r, nil
	}

	url := ratesURL + "&ccy=" + currency + "&dtFrom=" + date + "&dtTo=" + date

	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return r, fmt.Errorf("can't create request: %s", err.Error())
	}

	request.Header.Add("User-Agent", "go-currencies/0.0.1 https://gitlab.com/indikator/go-currencies")

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return r, fmt.Errorf("remote server error: %s", err.Error())
	}

	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return r, fmt.Errorf("error during getting currency rate: %s", err.Error())
	}

	var lbResponse struct {
		XMLName xml.Name `xml:"FxRates"`
		Rates   []struct {
			Currency string  `xml:"Ccy"`
			Rate     float64 `xml:"Amt"`
		} `xml:"FxRate>CcyAmt"`
	}

	err = xml.Unmarshal(body, &lbResponse)
	if err != nil {
		var lbErrResponse struct {
			XMLName xml.Name `xml:"FxRates"`
			Error   string   `xml:"OprlErr>Desc"`
		}

		err = xml.Unmarshal(body, &lbErrResponse)
		if err != nil {
			return r, fmt.Errorf("unexpected response data: %s", err.Error())
		}

		return r, errors.New(lbErrResponse.Error)
	}

	rates[currency+"."+date] = lbResponse.Rates[1].Rate
	r.Rate = rates[currency+"."+date]
	r.RunningTime = time.Since(timeStart).Seconds()

	return r, nil
}

// GetCurrencies - List of ISO 4217 currencies
func GetCurrencies() (map[string]string, error) {
	if currencies != nil {
		return currencies, nil
	}

	currencies = make(map[string]string)
	currencies["EUR"] = "Euro"

	request, err := http.NewRequest("GET", currenciesURL, nil)
	if err != nil {
		return nil, fmt.Errorf("can't create request: %s", err.Error())
	}

	request.Header.Add("User-Agent", "go-currencies/0.0.1 https://gitlab.com/indikator/go-currencies")

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return nil, fmt.Errorf("remote server error: %s", err.Error())
	}

	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, fmt.Errorf("error during getting currency rate: %s", err.Error())
	}

	var lbResponse struct {
		XMLName    xml.Name `xml:"CcyTbl"`
		Currencies []struct {
			Currency      string `xml:"Ccy"`
			CurrencyNames []struct {
				Lang string `xml:"lang,attr"`
				Name string `xml:",chardata"`
			} `xml:"CcyNm"`
		} `xml:"CcyNtry"`
	}

	err = xml.Unmarshal(body, &lbResponse)
	if err != nil {
		return nil, err
	}

	for _, c := range lbResponse.Currencies {
		currencies[c.Currency] = c.CurrencyNames[1].Name
	}

	return currencies, nil
}
